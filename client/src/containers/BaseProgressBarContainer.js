import React from "react";
import { useSelector } from "react-redux";
import BaseProgressBar from "../components/BaseProgressBar/BaseProgressBar";

const BaseProgressBarContainer = () => {
    const {category} = useSelector((state) => state.reducerCategory);
    const filteredCategory = category.filter((item) => item.checked);

    const completedCategory = (100 * filteredCategory.length / category.length);

    return <BaseProgressBar completedCategory={completedCategory}/> 
}

export default BaseProgressBarContainer;