import React, {/* useEffect, */ useState} from "react";
import AddTodo from "../components/AddTodo/AddTodo";
import { useDispatch } from "react-redux";
import { actionAddTodo/* ,getTodos */ } from "../store/todo";

const AddTodoContainer = () => {

    const [value,setValue] = useState('');
    const [isShow,setIsSHow] = useState(false);

    const dispatch = useDispatch();

    const handlerChange = (e) => {
        setValue(e.target.value);
    }

    const handlerAddTodo = () => {
        const data = {
            title:value,
            id: Date.now()
        }

        if (!value.length || /^\s/.test(value)) {
            return  setIsSHow(true)
        } else {setIsSHow(false)} 
        dispatch(actionAddTodo(data))
        setValue(""); 
    }

    //тесты useEffect
   /*  useEffect(()=>{
        console.log("useEffect like update method")
    })

    useEffect(()=> {
        console.log('useEffect like mount method')
        },[])
        useEffect(()=> {
            dispatch(getTodos());
            console.log('useEffect worked1')
        },[dispatch]) */

        return <AddTodo
         handlerAddTodo={handlerAddTodo}
         value={value}
         isShow={isShow}
         handlerChange={handlerChange}
         />
}

export default AddTodoContainer;