import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import CategoryLists from "../components/CategoryLists/CategoryLists";
import { useRouteMatch } from "react-router-dom";
import { actionCheckedCategory, actionEditStatusFiltered } from "../store/category";

const CategoryListsContainer = () => {
  const { category, statusFiltered } = useSelector(
    (state) => state.reducerCategory
  );
  const [categoryState, setCategoryState] = useState(category);

  const dispatch = useDispatch();
  const match = useRouteMatch("/:id");
  const matchTodoId = match?.params.id;

  const handlerChecked = (e, categoryId) => {
    const data = {
      categoryId,
      checked: e.target.checked,
    };
    dispatch(actionCheckedCategory(data));
  };

  useEffect(()=> {
      if (matchTodoId){
        dispatch(actionEditStatusFiltered('all'))
      }
  },[matchTodoId,dispatch])

  useEffect(() => {
    if (statusFiltered === "closed") {
      return setCategoryState(category.filter((item) => item.checked));
    }

    if (statusFiltered === "opened") {
      return setCategoryState(category.filter((item) => !item.checked));
    }

    setCategoryState(category);
  }, [category, statusFiltered]);

  return (
    <CategoryLists
      handlerChecked={handlerChecked}
      category={categoryState}
      matchTodoId={matchTodoId}
    />
  );
};

export default CategoryListsContainer;
