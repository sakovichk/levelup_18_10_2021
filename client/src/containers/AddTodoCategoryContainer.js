import React, { useState } from "react";
import { useDispatch} from "react-redux";
import { useRouteMatch } from "react-router-dom";
import AddTodoCategory from '../components/AddTodoCategory/AddTodoCategory';
import { actionAddCategory,actionEditStatusFiltered } from "../store/category";


const AddTodoCategoryContainer = () =>{
    const [value,setValue] = useState('');
    const [isShow,setIsSHow] = useState(false);
    const match = useRouteMatch('/:id');
    const matchTodoId = match?.params.id;

    const dispatch = useDispatch();

    const handlerChange = (e) => {
        setValue(e.target.value);
    }

    const handleAddCategory = () => {
        const data = {
            title:value,
            id:Date.now(),
            todoId:matchTodoId,
            checked:false,
        }
        if (!value.length || /^\s/.test(value) || !matchTodoId) {
            return  setIsSHow(true);
        } else {setIsSHow(false)} 
        dispatch(actionAddCategory(data));
        dispatch(actionEditStatusFiltered('all'));
        setValue("");
    }
    return <AddTodoCategory
        isShow={isShow}
        value={value}
        handlerChange={handlerChange}
        handleAddCategory={handleAddCategory}
        />
}

export default AddTodoCategoryContainer;