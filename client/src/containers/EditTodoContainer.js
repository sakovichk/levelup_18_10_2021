import React,{useState} from "react";
import { useDispatch } from "react-redux";
import EditTodo from "../components/EditTodo/EditTodo";
import { actionEditTodo } from "../store/todo";

const EditTodoContainer = ({title,todoId,handlerToggle}) => {
    const [value,setValue] = useState(title);
    const [isShowValidation,setIsShowValidation] = useState(false);
    const dispatch = useDispatch();

    const handlerChange = (e) => {
        setValue(e.target.value);
    }

    const handlerEditTodo = () => {
        const data  = {
            title:value,
            id:todoId,
        }
        if (!value.length || /^\s/.test(value)) {
            return  setIsShowValidation(true)
        } else {setIsShowValidation(false)}   
        dispatch(actionEditTodo(data));
        //закрывваю инпут edit
        handlerToggle();
    }
    
    return <EditTodo
    value={value} 
    handlerChange={handlerChange} 
    handlerEditTodo={handlerEditTodo}
    isShowValidation={isShowValidation}
    />
}

export default EditTodoContainer;