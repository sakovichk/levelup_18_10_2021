import React,{useState} from "react";
import SearchTodo from "../components/SearchTodo/SearchTodo";
import {useHistory} from "react-router-dom"

const SearchTodoContainer = () => {
    const [value,setValue] = useState("");
    const url = new URL(window.location.href);
    const history = useHistory();

    console.log(history);
 
    const handlerChange = (e) => {
        const setValueUrl = url.searchParams.set("searchTodo",`${e.target.value}`)
        history.replace(url.search.replace(setValueUrl))
        setValue(e.target.value)
    }
    


    return <SearchTodo value={value} handlerChange={handlerChange}/>
}

export default SearchTodoContainer;