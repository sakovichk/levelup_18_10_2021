import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import ToDoList from '../components/ToDoList/ToDoList'
import { actionDeleteTodo } from "../store/todo";


const ToDoListContainer = () => {
    const todoList = useSelector((state)=> state.reducerTodo);
    const [isShowEdit,setIsShowEdit] = useState(false);
    const [todoId,setTodoId] = useState('');

    //парсит url
    const location = useLocation();
    const getValueSearch = new URLSearchParams(location.search).get("searchTodo");

    const filteredTodoTitle = getValueSearch ? todoList.todo.filter((item) => item.title.toLocaleLowerCase().includes(getValueSearch.toLocaleLowerCase())) : todoList.todo;
    

    const dispatch = useDispatch();

    const handlerDeleteTodo = (id) => {
           dispatch(actionDeleteTodo(id));
    }
    
    const handlerToggle = (id) => {
        setTodoId(id)
        setIsShowEdit(!isShowEdit);
    }

    return <ToDoList 
    todoId={todoId}
    isShowEdit={isShowEdit}
    todoList={filteredTodoTitle}
    handlerToggle = {handlerToggle}
    handlerDeleteTodo={handlerDeleteTodo}
    />;
}

export default ToDoListContainer;