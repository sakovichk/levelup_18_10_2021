import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from "react-router-dom";
import App from './App';
import { configureStore } from './store';
import { Provider } from 'react-redux';
import './index.css';
import { ContextText } from './context/TextContext';

/* создаем store для хранения всех состояний приложения - принимает корневой reducer  в качестве аргумента и возвращет состояние 
 + вторым аргументом подключаем Redux Devtools для отслеживания состояний приложения - https://github.com/reduxjs/redux-devtools*/
const store = configureStore();

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <ContextText.Provider value={{text:"я контекст"}}>
        <Provider store={store}>
          <App />
        </Provider>
      </ContextText.Provider>
    </BrowserRouter>
  </React.StrictMode>,

  document.getElementById('root')
);


