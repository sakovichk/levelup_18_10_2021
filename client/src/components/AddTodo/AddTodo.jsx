import React from "react";
import { Button,InputGroup,FormControl } from "react-bootstrap";
import ErrorText from "../ErrorText/ErrorText";
import PropTypes,{func} from 'prop-types'
import "./style.css"

const AddTodo = ({value,isShow,handlerChange,handlerAddTodo}) => {
    
    return (
        <>
        <InputGroup className="mb-3 input" >
            <FormControl
                value = {value}
                onChange={handlerChange}
                placeholder="Write todo"
                aria-label="Recipient's username"
                aria-describedby="basic-addon2"
            />  
            <Button
            className="test-but"
            onClick={handlerAddTodo}
            variant="warning" 
            id="button-addon2"
            >
             Add
            </Button>
        </InputGroup>
        {isShow && <ErrorText/>}
        </>
            ) 
};

export default AddTodo;

AddTodo.propTypes = {
    value:PropTypes.string,
    isShow:PropTypes.bool,
    handlerChange:func,
    handlerAddTodo:func,
}

/* пример значения пропса по умолчанию
AddTodo.defaultProps = {
    value : "bla-bla"
} */