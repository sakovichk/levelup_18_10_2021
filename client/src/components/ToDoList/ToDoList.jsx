import React from "react";
/* import PropTypes from 'prop-types';  */
import EditTodoContainer from "../../containers/EditTodoContainer";
import { Link } from 'react-router-dom';
import { ListGroup,Button} from "react-bootstrap";
import './ToDoList.css';

const ToDoList = ({todoList,handlerDeleteTodo,handlerToggle,isShowEdit,todoId}) => {

    return (
        <ListGroup className="list">
          {todoList.length ? 
          todoList.map((item)=> {

           return (
              <ListGroup.Item
                key={item.id}
                variant="light"
                className="list-item-custom"
              >
              <div className="list_container">
              <Link to={`/${item.id}`}>
              <div className="list-title">
                {item.title}
              </div>
              </Link>
              <div className="buttons">
               <Button 
               //отключение кнопки 
               disabled={item.id === todoId ?isShowEdit :null }
               className = "test-but"
                variant="warning" 
                id="button-addon2"
                onClick={ () => handlerDeleteTodo(item.id)}
                >
                 Delete
                </Button>
                <Button 
                className="test-but"
                variant="warning" 
                id="button-addon2"
                onClick={ () => handlerToggle(item.id)}
                >
                 Edit
                </Button>
                </div>
                </div>
             {item.id === todoId ?
              isShowEdit && 
              <div className="edit-todo">
              <EditTodoContainer
               handlerToggle={handlerToggle}
               todoId={todoId}
               title={item.id===todoId ?
               item.title : ""}/></div> : null }      
             </ListGroup.Item>
            )}) : 
          <div className="warningContainer">
            <p className="warningOfEmptyList">The list is empty</p>
          </div>
        }
        </ListGroup>
            )
}

export default ToDoList;


/* ToDoList.propTypes = {
  todoList : PropTypes.object.isRequired,
  handlerDeleteTodo:PropTypes.func
} */
