import React from "react";
import './Link.css'

 const Link = ({title}) => {
    return <p className="menuItem">{title}</p>
}

export default Link;

