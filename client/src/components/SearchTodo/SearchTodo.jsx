import React from "react";
import {InputGroup,FormControl } from "react-bootstrap";

const SearchTodo = ({value,handlerChange}) => {
    return (
        <InputGroup className="mb-3 input" >
            <FormControl
                value = {value}
                onChange={handlerChange}
                placeholder="Search todo..."
                aria-label="Recipient's username"
                aria-describedby="basic-addon2"
            /> 
        </InputGroup>
    )
}

export default SearchTodo;