import React from "react";
import { ProgressBar } from "react-bootstrap";
import './BaseProgressBar.css'

const BaseProgressBar = ({completedCategory}) => {
  return <ProgressBar variant="success" animated now={completedCategory} className="progressBar-custom"/>;
};

export default BaseProgressBar;
