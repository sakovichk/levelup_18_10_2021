import React from "react";
import { ListGroup } from "react-bootstrap";
import CheckBox from "../CheckBox/CheckBox";
import "./CategoryLists.css";

const CategoryLists = ({ category, matchTodoId,handlerChecked}) => {
  return (
    <ListGroup className="list">
      {category.length ? (
        category.map((item) => {
          return (
            item.todoId === matchTodoId && (
              <ListGroup.Item
                key={item.id}
                variant="light"
                className="list-item-custom"
              >
                <div className="list_container">
                  {item.title}
                  <div>
                    <CheckBox 
                    checked={item.checked}
                     onChange={(e) => handlerChecked(e,item.id)}/>
                  </div>
                </div>
              </ListGroup.Item>
            )
          );
        })
      ) : (
        <div className="warningContainer">
          <p className="warningOfEmptyList">The list is empty</p>
        </div>
      )}
    </ListGroup>
  );
};

export default CategoryLists;
