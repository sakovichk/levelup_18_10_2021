import React from "react";
import "./ErrorText.css";

const ErrorText  = () => {
    return <h3 className="error">Invalid input</h3>
}

export default ErrorText;

