import React from "react";
import { ButtonGroup, ToggleButton } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { actionEditStatusFiltered } from "../../store/category/index";
import "./ButtonsFilteredCategory.css";

const buttons = [
  { name: "All", value: "1", filtered: "all" },
  { name: "Closed", value: "2", filtered: "closed" },
  { name: "Opened", value: "3", filtered: "opened" },
];

const ButtonsFilteredCategory = () => {

  const dispatch = useDispatch();

  const handlerButton = (args) => {
    dispatch(actionEditStatusFiltered(args));
  };

  return (
    <ButtonGroup className="button-group-custom">
      {buttons.map((item, idx) => (
        <ToggleButton
          key={idx}
          value={item.value}
          variant="warning"
          onClick={() => handlerButton(item.filtered)}
        >
          {item.name}
        </ToggleButton>
      ))}
    </ButtonGroup>
  );
};

export default ButtonsFilteredCategory;
