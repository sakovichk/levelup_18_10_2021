import React from "react";
import { useSelector } from "react-redux";

import { Button } from "react-bootstrap";
import "./SaveLocalStorage.css";

const SaveLocalStorage = () => {
  const {todo} = useSelector((state) => state.reducerTodo);
 
  const handleSaveLocalStorage = () => {
      const jsonStringifyTodo = JSON.stringify(todo);
      localStorage.setItem('todos',jsonStringifyTodo)
  };

  return (
    <Button
      onClick={handleSaveLocalStorage}
      className="save-local-storage-button"
      variant="warning"
      id="button-addon2"
    >
      Save to Local Storage
    </Button>
  );
};

export default SaveLocalStorage;
