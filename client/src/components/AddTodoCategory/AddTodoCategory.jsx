import React from "react";
import { Button,InputGroup,FormControl } from "react-bootstrap";
import ErrorText from "../ErrorText/ErrorText";

const AddTodoCategory = ({value,handlerChange,handleAddCategory,isShow}) => {
    return (
        <>
        <InputGroup className="mb-3 input" >
            <FormControl
                onChange={handlerChange}
                value={value}
                placeholder="Write todo category"
                aria-label="Recipient's username"
                aria-describedby="basic-addon2"
            />  
            <Button 
            onClick={handleAddCategory}      
            className="test-but"
            variant="warning" 
            id="button-addon2"
            >
             Add
            </Button>
        </InputGroup>
        {isShow && <ErrorText/>}
        </>
            ) 
}

export default AddTodoCategory;