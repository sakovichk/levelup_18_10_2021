import React from "react";
import { Button,InputGroup,FormControl } from "react-bootstrap";
import ErrorText from "../ErrorText/ErrorText";
import "./EditTodo.css"

const EditTodo = ( {value,handlerChange,handlerEditTodo,isShowValidation}) => {
    return (
        <>
        <div className="edit-container">
        <InputGroup className="mb-3 edit-main" >
            <FormControl
                onChange={handlerChange}
                value = {value}
                aria-label="Recipient's username"
                aria-describedby="basic-addon2"
            />  
            <Button
                onClick={handlerEditTodo}
                className="test-but"
                variant="warning" 
                id="button-addon2"
                >
                 Confirm
            </Button>
        </InputGroup>
            {/* <div className="cancel-button">
            <Button 
                className="test-but"
                variant="warning" 
                id="button-addon2"
                >
                Cancel
            </Button> 
            </div> */}
        </div>
        {isShowValidation && <ErrorText/>}
        </>            
    )
}

export default EditTodo;