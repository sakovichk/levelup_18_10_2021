import React from "react";
import "./CheckBox.css"

const CheckBox = ({ checked, onChange }) => {
  return (
    <input
      className="checkbox"
      type="checkbox"
      defaultChecked={checked}
      onChange={onChange}
    />
  );
};

export default CheckBox;
