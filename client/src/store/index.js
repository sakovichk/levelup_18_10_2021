//здесь собираем все reducer функций в один корневой rootReducer

import { combineReducers, createStore,applyMiddleware } from "redux";
import reducerTodo from "./todo/index";
import reducerCategory from "./category/index";
import thunk from "redux-thunk";
export * from "./todo/index"

export const rootReducer = combineReducers ({
    reducerTodo,
    reducerCategory
});

export const configureStore = (initState) => {
    const store = createStore(rootReducer,initState,applyMiddleware(thunk))
    return store;
}