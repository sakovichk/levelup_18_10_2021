const initState = {
  category: [],
  statusFiltered: 'all'
};

const actionType = {
  ADD_CATEGORY: "ADD_CATEGORY",
  CHECKED_CATEGORY: "CHECKED_CATEGORY",
  EDIT_STATUS_FILTERED : "EDIT_STATUS_FILTERED",
};

export const actionAddCategory = (payload) => ({
  type: actionType.ADD_CATEGORY,
  payload,
});

export const actionCheckedCategory = (payload) => ({
  type: actionType.CHECKED_CATEGORY,
  payload,
});

export const actionEditStatusFiltered = (payload) => ({
  type:actionType.EDIT_STATUS_FILTERED,
  payload
});

const reducerCategory = (state = initState, action) => {
  switch (action.type) {
    case actionType.ADD_CATEGORY:
      return { ...state, category: [...state.category, action.payload] };
    case actionType.CHECKED_CATEGORY:
      return {
        ...state,
        category: state.category.map((item) =>
          item.id === action.payload.categoryId
            ? { ...item, ...action.payload }
            : item
        ),
      };
      case actionType.EDIT_STATUS_FILTERED :
        return {
          ...state,statusFiltered:action.payload
        }
    default:
      return state;
  }
};

export default reducerCategory;
