const initState = {
  todo: [],
};

const actionType = {
  ADD_TODO: "ADD_TODO",
  DELETE_TODO: "DELETE_TODO",
  EDIT_TODO: "EDIT_TODO",
  GET_TODO_FROM_API: "GET_TODO_FROM_API",
  SAVE_LOCAL_STORAGE_TODO: "SAVE_LOCAL_STORAGE_TODO",
};

export const actionAddTodo = (payload) => ({
  type: actionType.ADD_TODO,
  payload,
});

export const actionDeleteTodo = (payload) => ({
  type: actionType.DELETE_TODO,
  payload,
});

export const actionGetTodoFromApi = (payload) => ({
  type: actionType.GET_TODO_FROM_API,
  payload,
});

export const actionSaveLocalStorage = (payload) => ({
  type: actionType.SAVE_LOCAL_STORAGE_TODO,
  payload,
});

const reducerTodo = (state = initState, action) => {
  switch (action.type) {
    case actionType.ADD_TODO:
      return { ...state, todo: [...state.todo, action.payload] };
    case actionType.DELETE_TODO:
      return {
        ...state,
        todo: state.todo.filter((item) => item.id !== action.payload),
      };
    case actionType.EDIT_TODO:
      return {
        ...state,
        todo: state.todo.map((item) =>
          item.id === action.payload.id ? { ...item, ...action.payload } : item
        ),
      };
    case actionType.GET_TODO_FROM_API:
      return { ...state, todo: [...state.todo, ...action.payload] };
    case actionType.SAVE_LOCAL_STORAGE_TODO:
      return { ...state, todo: [...state.todo, ...action.payload] };
    default:
      return state;
  }
};

export const actionEditTodo = (payload) => ({
  type: actionType.EDIT_TODO,
  payload,
});

export const getTodos = () => async (dispatch) => {
  try {
    const response = await fetch(
      "https://jsonplaceholder.typicode.com/todos?userId=1"
    );
    const data = await response.json();
    dispatch(actionGetTodoFromApi(data));
  } catch (error) {
    console.log(error);
  }
};

export default reducerTodo;
