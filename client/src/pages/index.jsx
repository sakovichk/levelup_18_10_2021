import React from "react";
import { Route,Routes } from "react-router-dom";

import Home from "./Home/Home";
import Contacts from "./Contacts/Contacts";
import HomeResourceCollection from './HomeResourceCollection/HomeResourceCollection'
import UserActions from "./UserActions/UserActions";

export const Page = () => {
    return <Routes>
        <Route path='/Home' element={<Home/>}/>
        <Route path='/Contacts' element={<Contacts/>}/>
        <Route path='/Home_Resource_Collection' element={<HomeResourceCollection/>}/>
        <Route path='/User_Actions' element={<UserActions/>}/>
    </Routes>
}
