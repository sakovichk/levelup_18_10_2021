import React, { useContext } from "react";
import { ContextText } from "../../context/TextContext";

const UserActions = () => {
    const context = useContext(ContextText)
    return <div>
         <h1>User Actions</h1>  
         <p>Action 1 {context.text}</p>
         <p>Action 2</p>
         <p>Action 3</p>
         <p>Action 4</p>
    </div>
}

export default UserActions;