import React from "react";

const HomeResourceCollection = () => {
    return <div>
         <h1>Home Resource Collection</h1>  
         <p>Collection 1</p>
         <p>Collection 2</p>
         <p>Collection 3</p>
         <p>Collection 4</p>
    </div>
}

export default HomeResourceCollection;