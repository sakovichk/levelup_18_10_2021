import React, { useEffect } from "react";
import MenuItem from "./components/Link/Link";
import { Link, useHistory } from "react-router-dom";
/* import { Page } from './pages'; */
import Container from "react-bootstrap/Container";
import AddTodoContainer from "./containers/AddTodoContainer";
import ToDoListContainer from "./containers/ToDoListContainer";
import AddTodoCategoryContainer from "./containers/AddTodoCategoryContainer";
import CategoryListsContainer from "./containers/CategoryListsContainer";
import ButtonsFilteredCategory from "./components/ButtonsFilteredCategory/ButtonsFilteredCategory";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import BaseProgressBarContainer from "./containers/BaseProgressBarContainer";
import SearchTodoContainer from "./containers/SearchTodoContainer";
import SaveLocalStorage from "./components/SaveLocalStorage/SaveLocalStorage";
import { useDispatch } from "react-redux";
import { actionSaveLocalStorage } from "./store/todo";

function App() {
  const history = useHistory();
  const dispatch = useDispatch();
  useEffect(() => {
    history.push("./");
    const getTodoLocalStorage = localStorage.getItem("todos");
    const parseTodo = JSON.parse(getTodoLocalStorage);
    if (parseTodo && parseTodo.length) {
      dispatch(actionSaveLocalStorage(parseTodo))
    }
  }, [history,dispatch]);

  return (
    <div className="container">
      <header className="menu">
        <Link to="/Home">
          <MenuItem title="Home" />
        </Link>
        <Link to="/User_Actions">
          <MenuItem title="User Actions" />
        </Link>
        <Link to="/Home_Resource_Collection">
          <MenuItem title="Home Resource Collection" />
        </Link>
        <Link to="/Contacts">
          <MenuItem title="Contacts" />
        </Link>
      </header>
      {/* <div className='content'><Page/></div> */}
      <div>
        <Container fluid>
          <div className="main">
            <div className="main-todo">
              <SearchTodoContainer/>
              <AddTodoContainer />
              <ToDoListContainer />
              <SaveLocalStorage/>
            </div>
            <div className="main-category">
              <BaseProgressBarContainer/>
              <AddTodoCategoryContainer />
              <ButtonsFilteredCategory />
              <CategoryListsContainer />
            </div>
          </div>
        </Container>
      </div>
    </div>
  );
}

export default App;
