const initState = {
  todos: [],
};

const actionType = {
  ADD_TODO: "ADD_TODO",
  DELETE_TODO: "DELETE_TODO",
  EDIT_TODO: "EDIT_TODO",
  LOCAL_STORAGE: "LOCAL_STORAGE",
};

export const actionAddTodo = (payload) => ({
  type: actionType.ADD_TODO,
  payload,
});

export const actionDeleteTodo = (payload) => ({
  type: actionType.DELETE_TODO,
  payload,
});

export const actionEditToDo = (payload) => ({
  type: actionType.EDIT_TODO,
  payload,
});

export const actionGetTodosFromLocalStorage = (payload) => ({
  type: actionType.LOCAL_STORAGE,
  payload,
});

const reducerTodos = (state = initState, action) => {
  switch (action.type) {
    case "ADD_TODO":
      return { ...state, todos: [...state.todos, action.payload] };
    case "DELETE_TODO":
      return {
        ...state,
        todos: state.todos.filter((item) => item.id !== action.payload),
      };
    case "EDIT_TODO":
      return {
        ...state,
        todos: state.todos.map((item) =>
          item.id === action.payload.id ? { ...item, ...action.payload } : item
        ),
      };
    case "LOCAL_STORAGE":
      return {
        ...state,
        todos: [...state.todos, ...action.payload],
      };
    default:
      return state;
  }
};

export default reducerTodos;
