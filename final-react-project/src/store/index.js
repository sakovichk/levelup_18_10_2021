import { combineReducers } from "redux";
import reducerTodos from "./todos";

export const rootReducer = combineReducers({
    reducerTodos
});