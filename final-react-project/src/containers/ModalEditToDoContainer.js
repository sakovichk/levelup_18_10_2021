import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import ModalEditToDo from "../components/ToDoList/ModalEditToDo/ModalEditToDo";
import { actionEditToDo } from "../store/todos/index";

const ModalEditToDoContainer = ({
  handlerModalEditToDo,
  itemIdForEdit,
  setIsShowModalEditTodo,
}) => {
  const toDoList = useSelector((state) => state.reducerTodos);
  const findId = toDoList.todos.find((item) => item.id === itemIdForEdit);

  const [title, setTitle] = useState(findId.title);
  const [text, setText] = useState(findId.text);
  const [isShowErrorMessage, setIsShowErrorMessage] = useState(false);
  const dispatch = useDispatch();

  const handlerEditTitle = (e) => {
    setTitle(e.target.value);
  };

  const handlerEditText = (e) => {
    setText(e.target.value);
  };

  const handlerEditToDo = () => {
    const data = {
      title: title,
      text: text,
      id: itemIdForEdit,
    };

    /* проверка: если название туду пустое или начинается на пробел - выводим предупреждение */
    if (!title.length || /^\s/.test(title)) {
      return setIsShowErrorMessage(true);
    } else {
      setIsShowErrorMessage(false);
    }

    /* отправляем новую тудушку в стор + обнуляем значения в инпутах */
    dispatch(actionEditToDo(data));
    console.log("data", data);
    console.log("toDoList", toDoList);
    setIsShowModalEditTodo(false);
  };

  return (
    <ModalEditToDo
      handlerModalEditToDo={handlerModalEditToDo}
      handlerEditTitle={handlerEditTitle}
      handlerEditText={handlerEditText}
      isShowErrorMessage={isShowErrorMessage}
      title={title}
      text={text}
      handlerEditToDo={handlerEditToDo}
    />
  );
};
export default ModalEditToDoContainer;
