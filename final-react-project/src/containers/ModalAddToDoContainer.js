import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { actionAddTodo } from "../store/todos/index";
import ModalAddToDo from "../components/ToDoList/ModalAddToDo/ModalAddToDo";

const ModalAddToDoContainer = ({
  setIsModalShow,
  handlerShowModal,
  isModalShow,
}) => {
  const [title, setTitle] = useState("");
  const [text, setText] = useState("");
  const [isShowErrorMessage, setIsShowErrorMessage] = useState(false);
  const dispatch = useDispatch();

  /* записываем в локальное состояние значение в инпуте для названия туду */
  const handlerChangeTitle = (e) => {
    setTitle(e.target.value);
  };

  /* записываем в локальное состояние значение в инпуте для текста туду */
  const handlerChangeText = (e) => {
    setText(e.target.value);
  };

  /* создаем объект с данными из инпутов для добавления в глобальное состояние - в массив с тудушками */
  const handlerAddToDo = () => {
    const data = {
      title: title,
      text: text,
      id: Date.now(),
    };

    /* проверка: если название туду пустое или начинается на пробел - выводим предупреждение */
    if (!title.length || /^\s/.test(title)) {
      return setIsShowErrorMessage(true);
    } else {
      setIsShowErrorMessage(false);
    }

    /* отправляем новую тудушку в стор + обнуляем значения в инпутах */
    dispatch(actionAddTodo(data));
  };

  return (
    <ModalAddToDo
      handlerChangeTitle={handlerChangeTitle}
      handlerChangeText={handlerChangeText}
      handlerAddToDo={handlerAddToDo}
      title={title}
      text={text}
      isShowErrorMessage={isShowErrorMessage}
      setIsModalShow={setIsModalShow}
      handlerShowModal={handlerShowModal}
      isModalShow={isModalShow}
    />
  );
};

export default ModalAddToDoContainer;
