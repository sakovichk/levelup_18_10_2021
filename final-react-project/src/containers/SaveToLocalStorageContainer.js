import React from "react";
import { useSelector } from "react-redux";
import SaveToLocalStorage from "../components/Footer/SaveToLocalStorage/SaveToLocalStorage";

const SaveToLocalStorageContainer = () => {
  const toDoList = useSelector((state) => state.reducerTodos.todos);

  /* сохраняем в локал сторидж текущее состояние массива с тудушками, предварительно зачищаем локал сторидж от todos*/
  const handlerSaveToLocalStorage = () => {
    localStorage.removeItem("todos");
    const stringTodos = JSON.stringify(toDoList);
    localStorage.setItem("todos", stringTodos);
  };

  /* зачистка локал сториджа по кнопке */
  const handlerClearLocalStorage = () => {
    localStorage.removeItem("todos");
  };

  return (
    <SaveToLocalStorage
      handlerSaveToLocalStorage={handlerSaveToLocalStorage}
      handlerClearLocalStorage={handlerClearLocalStorage}
    />
  );
};

export default SaveToLocalStorageContainer;
