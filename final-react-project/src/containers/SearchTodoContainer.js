import React, { useState } from "react";
import SearchTodo from "../components/Header/SearchToDo/SearchToDo";
import { useNavigate } from "react-router-dom";

const SearchTodoContainer = () => {
  const [value, setValue] = useState("");

  /* записываем в url - значение из инпута поиска */
  const url = new URL(window.location.href);
  const navigate = useNavigate();

  const handlerChangeSearch = (e) => {
    setValue(e.target.value);
  };

  const handlerSearchToDo = () => {
    const setValueUrl = url.searchParams.set("searchTodo", `${value}`);
    navigate(url.search.replace(setValueUrl));
  };

  return (
    <SearchTodo
      handlerChangeSearch={handlerChangeSearch}
      value={value}
      handlerSearchToDo={handlerSearchToDo}
    />
  );
};

export default SearchTodoContainer;
