import React, { useState } from "react";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import ToDoList from "../components/ToDoList/ToDoList";

const ToDoListContainer = () => {
  /* получаем состояние из стора */
  const toDoList = useSelector((state) => state.reducerTodos);

  /*  функционал фильтрации тудулиста */
  const location = useLocation();
  const getValueSearch = new URLSearchParams(location.search).get("searchTodo");

  const filteredTodoList = getValueSearch
    ? toDoList.todos.filter((item) =>
        item.title
          .toLocaleLowerCase()
          .includes(getValueSearch.toLocaleLowerCase())
      )
    : toDoList.todos;

  /* управлением модальным окном для добавления тудушки */
  const [isModalShow, setIsModalShow] = useState(false);

  const handlerShowModal = () => {
    setIsModalShow(!isModalShow);
  };

  /*  управлением модальным окно для подтверждения удаления тудушки */
  const [isShowModalDeleteToDo, setIsShowModalDeleteToDo] = useState(false);

  const [itemId, setItemId] = useState("");

  const handlerShowModalDeleteToDo = (itemId) => {
    setItemId(itemId);
    setIsShowModalDeleteToDo(!isShowModalDeleteToDo);
  };

  /* управление модальным окно для редактирования тудушки  */
  const [itemIdForEdit, setItemIdForEdit] = useState("");

  const [isShowModalEditToDo, setIsShowModalEditTodo] = useState(false);

  const handlerModalEditToDo = (itemId) => {
    setItemIdForEdit(itemId);
    setIsShowModalEditTodo(!isShowModalEditToDo);
  };

  return (
    <ToDoList
      toDoList={filteredTodoList}
      isModalShow={isModalShow}
      setIsModalShow={setIsModalShow}
      handlerShowModal={handlerShowModal}
      handlerShowModalDeleteToDo={handlerShowModalDeleteToDo}
      isShowModalDeleteToDo={isShowModalDeleteToDo}
      itemId={itemId}
      handlerModalEditToDo={handlerModalEditToDo}
      setIsShowModalEditTodo={setIsShowModalEditTodo}
      itemIdForEdit={itemIdForEdit}
      isShowModalEditToDo={isShowModalEditToDo}
    />
  );
};

export default ToDoListContainer;
