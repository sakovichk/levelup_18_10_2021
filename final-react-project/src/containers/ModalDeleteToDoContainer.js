import React from "react";
import { useDispatch } from "react-redux";
import ModalDeleteToDo from "../components/ToDoList/ModaleDeleteToDo/ModalDeleteToDo";
import { actionDeleteTodo } from "../store/todos";

const ModalDeleteToDoContainer = ({
  handlerShowModalDeleteToDo,
  isShowModalDeleteToDo,
  itemId,
}) => {
  const dispatch = useDispatch();

  const handlerDeleteTodo = () => {
    dispatch(actionDeleteTodo(itemId));
    handlerShowModalDeleteToDo();
  };

  return (
    <ModalDeleteToDo
      handlerShowModalDeleteToDo={handlerShowModalDeleteToDo}
      handlerDeleteTodo={handlerDeleteTodo}
      isShowModalDeleteToDo={isShowModalDeleteToDo}
    />
  );
};

export default ModalDeleteToDoContainer;
