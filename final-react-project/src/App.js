import { useDispatch } from "react-redux";
import { useEffect } from "react";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import ToDoListContainer from "./containers/ToDoListContainer";
import { actionGetTodosFromLocalStorage } from "./store/todos/index";
import "./App.css";

function App() {
  const dispatch = useDispatch();

  /* перед монитированием получаем тудушки из local storage, если он не пустой */
  useEffect(() => {
    const getLocalStorageTodos = JSON.parse(localStorage.getItem("todos"));
    if (getLocalStorageTodos && getLocalStorageTodos.length) {
      dispatch(actionGetTodosFromLocalStorage(getLocalStorageTodos));
    }
  }, [dispatch]);

  return (
    <>
      <Header />
      <ToDoListContainer />
      <Footer />
    </>
  );
}

export default App;
