import React from "react";
import s from "./ToDo.module.css";

const ToDo = ({
  title,
  text,
  handlerShowModalDeleteToDo,
  itemId,
  handlerModalEditToDo,
  itemIdForEdit,
}) => {
  return (
    <div className={s.container}>
      <div className={s.todo_item}>
        <div
          className={s.todo_title}
          onClick={() => handlerModalEditToDo(itemIdForEdit)}
        >
          {title}
        </div>
        <div className={s.todo_text}>
          <div
            className={s.todo_text_value}
            onClick={() => handlerModalEditToDo(itemIdForEdit)}
          >
            {text}
          </div>
          <div>
            <button
              className={s.button}
              onClick={() => handlerShowModalDeleteToDo(itemId)}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                height="20px"
                viewBox="0 0 24 24"
                width="20px"
                fill="#000000"
              >
                <path d="M0 0h24v24H0V0z" fill="none" />
                <path d="M16 9v10H8V9h8m-1.5-6h-5l-1 1H5v2h14V4h-3.5l-1-1zM18 7H6v12c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7z" fill="white"/>
              </svg>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ToDo;
