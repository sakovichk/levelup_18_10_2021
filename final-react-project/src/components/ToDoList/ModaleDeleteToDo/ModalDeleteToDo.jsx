import React from "react";
import Button from "../../Button/Button";
import "./ModalDeleteToDo.css";

const ModalDeleteToDo = ({
  handlerShowModalDeleteToDo,
  handlerDeleteTodo,
  isShowModalDeleteToDo,
}) => {
  return (
    <div className={`wrapper ${isShowModalDeleteToDo ? "open" : "close"}`}>
      <div className={"container_del"}>
        <div className={"dialog_content_del"}>
          Аre you sure you want to delete this task?
        </div>
        <div className={"buttons_del"}>
          <Button title="Yes" onClick={handlerDeleteTodo} />
          <Button title="No" onClick={handlerShowModalDeleteToDo} />
        </div>
      </div>
    </div>
  );
};

export default ModalDeleteToDo;
