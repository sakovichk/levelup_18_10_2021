import React from "react";
import s from "./ErrorMessage.module.css";

const ErrorMessage = (props) => {
  return <div className={s.message}>{props.text}</div>;
};

export default ErrorMessage;
