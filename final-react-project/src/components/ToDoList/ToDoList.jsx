import React, { useEffect } from "react";
import ErrorMessage from "./ErrorMessage/ErrorMessage";
import ToDo from "./ToDo/ToDo";
import ModalAddToDoContainer from "../../containers/ModalAddToDoContainer";
import ModalDeleteToDoContainer from "../../containers/ModalDeleteToDoContainer";
import ModalEditToDoContainer from "../../containers/ModalEditToDoContainer";
import s from "./ToDoList.module.css";

const ToDoList = ({
  toDoList,
  isModalShow,
  setIsModalShow,
  handlerShowModal,
  handlerShowModalDeleteToDo,
  isShowModalDeleteToDo,
  itemId,
  handlerModalEditToDo,
  isShowModalEditToDo,
  itemIdForEdit,
  setIsShowModalEditTodo,
}) => {
  // в случае успешного добавления новой тудушки, удаляем модальное окно для добавления туду
  useEffect(() => {
    setIsModalShow(false);
  }, [toDoList, setIsModalShow]);

  return (
    <div className={s.container}>
      {/* модальное окно для редактирования тудушки */}
      {isShowModalEditToDo && (
        <ModalEditToDoContainer
          handlerModalEditToDo={handlerModalEditToDo}
          itemIdForEdit={itemIdForEdit}
          setIsShowModalEditTodo={setIsShowModalEditTodo}
        />
      )}

      {/* модальное окно для удаления тудушки */}
      {isShowModalDeleteToDo && (
        <ModalDeleteToDoContainer
          handlerShowModalDeleteToDo={handlerShowModalDeleteToDo}
          isShowModalDeleteToDo={isShowModalDeleteToDo}
          itemId={itemId}
        />
      )}

      {/* выводим модально окно для добавления туду */}
      {isModalShow && (
        <ModalAddToDoContainer
          handlerShowModal={handlerShowModal}
          isModalShow={isModalShow}
        />
      )}

      {/* если модального окна туду нет, выводим кнопку для добавления */}
      {!isModalShow && (
        <button onClick={handlerShowModal} className={s.button}>
          +
        </button>
      )}

      {/* отрисовываем массив тудушек если не он пустой, иначе выводим предупреждение */}
      {toDoList.length ? (
        toDoList.map((item, index) => {
          return (
            <ToDo
              key={index}
              title={item.title}
              text={item.text}
              handlerShowModalDeleteToDo={handlerShowModalDeleteToDo}
              itemId={item.id}
              handlerModalEditToDo={handlerModalEditToDo}
              itemIdForEdit={item.id}
              setIsShowModalEditTodo={setIsShowModalEditTodo}
            />
          );
        })
      ) : (
        <div className={s.error_message}>
          <ErrorMessage text="There are not any todos" />
        </div>
      )}
    </div>
  );
};

export default ToDoList;
