import React from "react";
import "./ModalAddToDo.css";
import Button from "../../Button/Button";
import ErrorMessage from "../ErrorMessage/ErrorMessage";

const ModalAddToDo = ({
  handlerChangeTitle,
  handlerChangeText,
  handlerAddToDo,
  title,
  text,
  isShowErrorMessage,
  handlerShowModal,
  isModalShow,
}) => {
  return (
    <div className={`wrapper ${isModalShow ? "open" : "close"}`}>
      <div className={`container`}>
        <div className={"input_group"}>
          <div className={"input_todo_name"}>
            <div>Add todo title</div>
            <input
              maxLength="30"
              value={title}
              onChange={handlerChangeTitle}
              type="text"
              placeholder="Input title"
              className={"input"}
            />
          </div>
          <div className={"input_todo_text"}>
            <div>Add todo text</div>
            <input
              value={text}
              onChange={handlerChangeText}
              type="text"
              placeholder="Input text"
              className={"input"}
            />
          </div>
        </div>
        <div className={"footer"}>
          <div>
            {isShowErrorMessage && <ErrorMessage text="PLEASE ADD TITLE" />}
          </div>
          <div className={"buttons"}>
            <div className={"button"}>
              <Button title="Create" onClick={handlerAddToDo} />
            </div>
            <div className={"button"}>
              <Button title="Cancel" onClick={handlerShowModal} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModalAddToDo;
