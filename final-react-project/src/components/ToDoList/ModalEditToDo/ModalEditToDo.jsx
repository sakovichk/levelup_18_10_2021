import React from "react";
import Button from "../../Button/Button";
import ErrorMessage from "../ErrorMessage/ErrorMessage";

const ModalEditToDo = ({
  handlerModalEditToDo,
  handlerEditTitle,
  handlerEditText,
  isShowErrorMessage,
  title,
  text,
  handlerEditToDo,
}) => {
  return (
    <div className={`wrapper ${true ? "open" : "close"}`}>
      <div className={`container`}>
        <div className={"input_group"}>
          <div className={"input_todo_name"}>
            <div>Edit todo title</div>
            <input
              maxLength="30"
              onChange={handlerEditTitle}
              type="text"
              placeholder="Input title"
              className={"input"}
              value={title}
            />
          </div>
          <div className={"input_todo_text"}>
            <div>Edit todo text</div>
            <input
              type="text"
              placeholder="Input text"
              className={"input"}
              onChange={handlerEditText}
              value={text}
            />
          </div>
        </div>
        <div className={"footer"}>
          <div>
            {isShowErrorMessage && <ErrorMessage text="PLEASE ADD TITLE" />}
          </div>
          <div className={"buttons"}>
            <div className={"button"}>
              <Button title="Confirm" onClick={handlerEditToDo} />
            </div>
            <div className={"button"}>
              <Button title="Cancel" onClick={handlerModalEditToDo} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModalEditToDo;
