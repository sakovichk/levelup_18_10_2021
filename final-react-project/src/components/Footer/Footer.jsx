import React from "react";
import SaveToLocalStorageContainer from "../../containers/SaveToLocalStorageContainer";
import s from "./Footer.module.css";

const Footer = () => {
  return (
    <div className={s.container}>
      <SaveToLocalStorageContainer />
    </div>
  );
};

export default Footer;
