import React from "react";
import Button from "../../Button/Button";

const SaveToLocalStorage = ({handlerSaveToLocalStorage,handlerClearLocalStorage}) => {
    return (
        <>
        <Button title = "Save to Local Storage" onClick ={handlerSaveToLocalStorage}/>
        <Button title = "Clear Local Storage" onClick ={handlerClearLocalStorage}/>
        </>
    )
}

export default SaveToLocalStorage;