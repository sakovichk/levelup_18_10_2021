import React from "react";
import s from "./Button.module.css";

const Button = (props) => {
  return (
    <button onClick={props.onClick} className={s.button}>
      {props.title}
    </button>
  );
};

export default Button;
