import React from "react";
import SearchTodoContainer from "../../containers/SearchTodoContainer";
import s from "./Header.module.css";

const Header = () => {
  return (
    <div className={s.container}>
      <SearchTodoContainer />
    </div>
  );
};

export default Header;
