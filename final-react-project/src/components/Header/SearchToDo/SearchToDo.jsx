import React from "react";
import s from './SeatchToDo.module.css'
import Button from '../../Button/Button'

const SearchTodo = ({value,handlerChangeSearch,handlerSearchToDo}) => {
  return (
    <div className={s.container}>
      <input value = {value} onChange = {handlerChangeSearch} type="text" placeholder="search..." className={s.input} />
      <Button title="search" onClick={handlerSearchToDo}/>
    </div>
  );
};

export default SearchTodo;
